const express = require('express');
const app       = express();
const cors = require('cors');
app.use(cors());


app.get('/',(req, res)=>{
    console.log("CHAMADO VIA GET");
    res.send("Method GET - Route: /");
});
app.get('/usuario',(req, res)=>{
    console.log("CHAMADO VIA GET - consulta usuario");
    res.send("Method GET - Route: /usuario");
});
app.get('/cliente',(req, res)=>{
    console.log("CHAMADO VIA GET - consulta cliente");
    console.log(req.query);
    let nome = req.query.fname;
    let sobrenome = req.query.lname;
     res.send("Nome: " + nome + " - Sobrenome");
});

app.post('/cliente',(req, res)=>{
    console.log("Method POST - Route: /cliente");
});


app.listen(3000, function () { 
    console.log("projeto iniciado na porta 3000")

});